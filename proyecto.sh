#!/bin/bash

conceptos_folder="Conceptos"

if [ ! -d "$conceptos_folder" ]; then
    mkdir "$conceptos_folder"
fi

mostrar_menu1() {
    echo "Bienvenido a la guía rápida de Agile, para continuar seleccione un tema:"
    echo "1. SCRUM"
    echo "2. X.P."
    echo "3. Kanban"
    echo "4. Crystal"
}

mostrar_menu2() {
    echo "Bienvenido a la guía rápida de metodologías tradicionales, para continuar seleccione un tema:"
    echo "1. Cascada"
    echo "2. Espiral"
    echo "3. Modelo V"
}

submenu() {
    local seccion="$1"
    echo "Usted esta en la sección $seccion, seleccione la opción que desea utilizar: "
    echo "1. Agregar información"
    echo "2. Buscar información"
    echo "3. Eliminar información"
    echo "4. Leer base de información"
    echo "5. Volver al menú anterior"
    echo "6. Finalizar el programa"
}

agregar_info() {
    local seccion="$1"
    echo "Ingrese el nombre del concepto:"
    read nombre
    echo "Ingrese la definición:"
    read definicion
    echo "[$nombre].- $definicion" >> "$conceptos_folder/$seccion.inf"
    echo "Información agregada correctamente."
}

buscar_info() {
    local seccion="$1"
    echo "Ingrese el concepto a buscar:"
    read concept
    if grep -q "\[$concept\]" "$conceptos_folder/$seccion.inf"; then
        grep -o "\[$concept\].*" "$conceptos_folder/$seccion.inf" | sed 's/^\[.*\] //'
    else
        echo "El concepto \"$concept\" no se encontró en la sección \"$seccion\"."
    fi
}

eliminar_info() {
    local seccion="$1"
    echo "Ingrese el concepto a eliminar:"
    read concept
    if grep -q "\[$concept\]" "$conceptos_folder/$seccion.inf"; then
        sed -i "/\[$concept\]/d" "$conceptos_folder/$seccion.inf"
        echo "Información eliminada correctamente."
    else
        echo "El concepto \"$concept\" no se encontró en la sección \"$seccion\"."
    fi
}

leer_info() {
    local seccion="$1"
    echo "Contenido del archivo $conceptos_folder/$seccion.inf:"
    cat "$conceptos_folder/$seccion.inf"
}

while true; do
    if [ "$1" = "-a" ]; then

        mostrar_menu1
        read opcion

        case $opcion in
            1) seccion="scrum";;
            2) seccion="xp";;
            3) seccion="kanban";;
            4) seccion="crystal";;
            *) echo "Opción inválida"; continue;;
        esac

        while true; do
            submenu $seccion
            read opcion

            case $opcion in
                1) agregar_info $seccion;;
                2) buscar_info $seccion;;
                3) eliminar_info $seccion;;
                4) leer_info $seccion;;
                5) break;;
                6) exit;;
                *) echo "Opción inválida";;
            esac
        done

    elif [ "$1" = "-t" ]; then

        mostrar_menu2
        read opcion

        case $opcion in
            1) seccion="cascada";;
            2) seccion="espiral";;
            3) seccion="modelo_v";;
            *) echo "Opción inválida"; continue;;
        esac

        while true; do
            submenu $seccion
            read opcion

            case $opcion in
                1) agregar_info $seccion;;
                2) buscar_info $seccion;;
                3) eliminar_info $seccion;;
                4) leer_info $seccion;;
                5) break;;
                6) exit;;
                *) echo "Opción inválida";;
            esac
        done

    else
        echo "Debes agregar el parámetro '-a' o el parámetro '-t' para ejecutar correctamente el archivo"
        exit 1
    fi
done
