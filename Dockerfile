FROM ubuntu:latest

RUN apt update && \
    apt install -y nmap git && \
    apt autoremove -y

RUN git clone https://github.com/andy462/ProyectoPrimerParcial.git

WORKDIR /ProyectoPrimerParcial

COPY proyecto.sh /ProyectoPrimerParcial/
RUN chmod +x /ProyectoPrimerParcial/proyecto.sh

CMD ["./proyecto.sh"]
