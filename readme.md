# Proyecto Primer Parcial

Este programa en bash te proporciona una manera rápida de acceder a información sobre metodologías ágiles y tradicionales de desarrollo de software.

## Descripción

Este script desplegará un menú que te permite seleccionar entre metodologías ágiles y tradicionales. Luego, puedes elegir una sección específica dentro de la metodología seleccionada para agregar información, buscar, eliminar información o leer la base de información.

## Inicio

### Dependencias

* Este script se ejecuta en un entorno bash estándar.

### Instalación

1. Descarga el script `proyecto.sh`.
2. Otorga permisos de ejecución al script si es necesario:
   ```
   chmod 755 proyecto.sh
   ```

### Ejecución del programa

1. Abre una terminal.
2. Navega hasta la ubicación donde guardaste el script.
3. Ejecuta el siguiente comando (la x debe ser reemplazada por a para metodologías ágiles y t para metodologías tradicionales):
   ```
   sh proyecto.sh -x
   ```

## Uso

1. Seleccine una sección.
2. Una vez dentro de una sección, podrás seleccionar entre varias opciones para agregar información, buscar, eliminar información o leer la base de información. También puedes regresar a seleccionar una sección si así lo deseas.

## Autores

* Andrea Burciaga
* Danna Corral
* Luis Moncayo

## Historial de Versiones

* 1.0
    * Versión inicial del script.
